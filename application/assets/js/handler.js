import {
  viewer
} from "./viewer"

export var handler = {
  svgs: document.getElementsByTagName("object"),
  spans: document.querySelectorAll("span[data-location]"),
  spansInDoc: document.querySelectorAll("#roman>span[data-location]"),
  titlesLocation: document.querySelectorAll("span.title-location"),
  spanInfo: document.getElementById("info-count-location"),
  previousLocationId: null,
  currentLocationId: null,
  currentGlobalIndex: null,
  currentLocalIndex: null,
  currentLocalSpan: null,
  currentImage: null,
  init: function() {
    $("#locations-choice").on('input', function() {
      var opt = $('#locations-choices option[value="' + $(this).val() + '"]');
      if (opt.length) {
        let locationId = opt.attr("data-location")
        handler.toggleLocation(locationId);
      }
    });

    [].forEach.call(this.svgs, function(svg) {
      var mysvg = svg;
      var svgDoc = mysvg.contentDocument;
      var els = svgDoc.querySelectorAll("g");
      [].forEach.call(els, function(el) {
        el.addEventListener('click', function() {
          var locationId = el.getAttribute("data-location");
          handler.toggleLocation(locationId);
        });

      });
    });

    // add click event on text
    Array.from(this.spansInDoc).forEach(function(el) {
      var locationId = el.getAttribute("data-location");
      el.addEventListener('click', () => {
        handler.toggleLocation(locationId);
      });
    });

    $(".image-modal").on('click', function() {
      let img = $("#img-to-display");
      let url = $(this).attr("data-url");
      let imageName = $(this).attr("data-imagename");
      handler.currentImage = imageName;
      let btnDownload = $("#osd-download");

      img.attr("src", "");
      $('#modal-image-title').html(imageName);
      btnDownload.attr("href", "/data/images/" + imageName);
      viewer.open({
        type: 'image',
        url: url
      });
      $('#modal-image').modal('show');
    });
  },

  displayLocation: function(locationId) {
    //display Images & Spans just for global
    [].forEach.call(this.svgs, function(svg) {
      var svgDoc = svg.contentDocument;
      var carres = svgDoc.querySelectorAll("g");
      var atLeastOne = false;

      Array.from(carres).forEach(function(carre) {
        if (carre.getAttribute("data-location") == locationId) {
          atLeastOne = true;
          carre.classList.remove('location-hidden');
        } else {
          carre.classList.add('location-hidden');
        }
      });
      svg.parentNode.parentNode.style.display = (!atLeastOne) ? "none" : "inline-block";
    });

    Array.from(this.spans).forEach(function(span) {
      if (span.getAttribute("data-location") == locationId) {
        span.classList.add('highlighted');
      } else {
        span.classList.remove('highlighted');
      }
    });
  },

  toggleLocation: function(locationId) {
    handler.currentLocationId = locationId;

    if (this.previousLocationId == this.currentLocationId) {
      this.reset();
    } else {
      this.setInputValue();
      this.previousLocationId = this.currentLocationId;

      //affichage contour carré dans svgs et masquage des images sans rien
      this.handlerImages();
      //highlight des span pour titre et dans roman
      this.handlerSpans();
      // si c'est une image faut scroller jusqu'à la première mention
      this.scrollTo(locationId);
      // afficher des boutons next/previous le rang et le total d'occurrence pour le currentLocationIdlocationId

      //Injection du current id dans le title de la modal
      $('#modal-current-location').html(handler.getValueFromId());
    }
  },

  getCountByLocation: function(index, total) {
    let str = (total == 0) ? "" : index + 1 + "/" + total;

    return str;
  },

  scrollTo: function(where) {
    var currentElement = null;
    var countElements = null;
    var index = null;
    // Global
    if (this.currentLocationId === null) {
      // 1er scroll en global
      //Récupérer l'index global du courant local
      //this.currentLocalIndex = 0;
      var spans = Array.from(this.spansInDoc);
      if (this.currentGlobalIndex == null) {
        this.currentGlobalIndex = 0;
      } else {
        if (where == "next") {
          this.currentGlobalIndex = (this.currentGlobalIndex >= spans.length - 1) ? 0 : this.currentGlobalIndex + 1;
        } else {
          this.currentGlobalIndex = (this.currentGlobalIndex == 0) ? spans.length - 1 : this.currentGlobalIndex - 1;
        }
      }
      countElements = spans.length;
      currentElement = spans[this.currentGlobalIndex];
      index = this.currentGlobalIndex;
      var currentElementLocationId = currentElement.getAttribute("data-location");
      this.displayLocation(currentElementLocationId);

      handler.setInputValue(currentElementLocationId);
    }
    // Local
    else {
      this.currentGlobalIndex = 0;
      var localSpans = Array.from(document.querySelectorAll("#roman span[data-location='" + this.currentLocationId + "']"));
      if (this.currentLocalIndex === null) {
        currentElement = localSpans[0];
        this.currentLocalIndex = 0;
      } else {
        if (where == "next") {
          this.currentLocalIndex = (this.currentLocalIndex >= localSpans.length - 1) ? 0 : this.currentLocalIndex + 1;
        } else if (where == "previous") {
          this.currentLocalIndex = (this.currentLocalIndex == 0) ? (localSpans.length - 1) : this.currentLocalIndex - 1;
        } else { //cas clique sur image ou titre lieux -> scroll to 1st mention

          var localSpans = Array.from(document.querySelectorAll("#roman span[data-location='" + where + "']"));
          currentElement = localSpans[0];
          this.currentLocalIndex = 0;
        }
      }
      index = this.currentLocalIndex;
      countElements = localSpans.length;
      currentElement = localSpans[this.currentLocalIndex];
    }

    if (currentElement) {
      currentElement.scrollIntoView();
      var scrolledY = window.scrollY;
      window.scroll(0, scrolledY - 350);
    }
    this.spanInfo.innerHTML = this.getCountByLocation(index, countElements);
  },

  getValueFromId: function() {
    var opt = $('#locations-choices option[data-location="' + handler.currentLocationId + '"]');
    if (opt.length) {
      return opt.attr("value")

    }
    return;
  },

  setInputValue: function(id) {
    var opt = $('#locations-choices option[data-location="' + id + '"]');
    if (opt.length) {
      $('#locations-choice').val(opt.attr("value"))
    }
  },

  handlerSpans: function() {
    this.setInputValue(handler.currentLocationId);
    // gestion surligné ou non des segments de texte + remplissage input du locationId Courant

    Array.from(this.spans).forEach(function(span) {
      if (span.getAttribute("data-location") == handler.currentLocationId) {
        span.classList.add('highlighted');
      } else {
        span.classList.remove('highlighted');
      }
    });
  },

  handlerImages: function() {
    // cache les images qu'on pas de carrés affichables
    // contour des carrés matchant currentLocationId
    [].forEach.call(this.svgs, function(svg) {
      var svgDoc = svg.contentDocument;
      var carres = svgDoc.querySelectorAll("g");
      var atLeastOne = false;

      Array.from(carres).forEach(function(carre) {
        if (carre.getAttribute("data-location") == handler.currentLocationId) {
          atLeastOne = true;
          carre.classList.remove('location-hidden');
        } else {
          carre.classList.add('location-hidden');
        }
      });

      svg.parentNode.parentNode.style.display = (!atLeastOne) ? "none" : "inline-block";
    });

    Array.from(document.querySelectorAll(".overlay, .overlay-active")).forEach(function(overlay) {
      if (overlay.getAttribute("title") == handler.currentLocationId) {
        overlay.classList.add("overlay-active")
        overlay.classList.remove("overlay")
      } else {
        overlay.classList.add("overlay")
        overlay.classList.remove("overlay-active")
      }
    });
  },

  reset: function() {
    this.spanInfo.innerHTML = "";
    this.previousLocationId = null;
    this.currentLocationId = null;
    document.getElementById("locations-choice").value = "";
    $('#modal-current-location').html("");

    // on afffiche les images
    [].forEach.call(this.svgs, function(svg) {
      svg.parentNode.parentNode.style.display = "inline-block";
      var svgDoc = svg.contentDocument;
      var els = svgDoc.querySelectorAll("g");
      [].forEach.call(els, function(el) {
        // on masque les carrés
        el.classList.add("location-hidden")
      });
    });
    Array.from(document.querySelectorAll(".overlay-active")).forEach(function(overlay) {
      overlay.classList.remove('overlay-active')
      overlay.classList.add('overlay')
    });

    //reset de la coloration textuelle (roman et lien lieux)
    Array.from(this.spans).forEach(function(span) {
      span.classList.remove('highlighted');
    });
    $('#modal-current-location').html();
  }
};