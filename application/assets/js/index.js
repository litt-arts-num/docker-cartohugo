import {
  romanHandler
} from './roman'

import {
  handler
} from './handler'

document.addEventListener("DOMContentLoaded", function(event) {
  handler.init()
});

document.getElementById("zoom-out").addEventListener("click", function(event) {
  var count = document.getElementById("gallery").style.columnCount;
  if (count < 3) {
    document.getElementById("gallery").style.columnCount = parseInt(count, 10) + 1;
  }
}, false);

document.getElementById("zoom-in").addEventListener("click", function(event) {
  var count = document.getElementById("gallery").style.columnCount;
  if (count > 1) {
    document.getElementById("gallery").style.columnCount = count - 1;
  }
}, false);

document.getElementById("expan").addEventListener("click", () => {
  romanHandler.expan()
}, false);


document.getElementById("collapse").addEventListener("click", () => {
  romanHandler.collapse()
}, false)

document.getElementById("previous").addEventListener('click', () => {
  handler.scrollTo("previous")
})

document.getElementById("reset").addEventListener('click', () => {
  handler.reset()
})

document.getElementById("next").addEventListener('click', () => {
  handler.scrollTo("next")
})