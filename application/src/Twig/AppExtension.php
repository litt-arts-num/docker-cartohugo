<?php
namespace App\Twig;

use App\Entity\Parameters;
use App\Manager\CacheManager;
use Doctrine\ORM\EntityManagerInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class AppExtension extends AbstractExtension
{
    private $em;
    private $cm;

    public function __construct(EntityManagerInterface $em, CacheManager $cm)
    {
        $this->em = $em;
        $this->cm = $cm;
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('getParameters', [$this, 'getParameters']),
        ];
    }

    public function getParameters()
    {
        $parametersCache = $this->cm->get("parameters");

        if ($parametersCache->isHit()) {
            $parameters = $parametersCache->get();
        } else {
            $parameters = $this->em->getRepository(Parameters::class)->findOneBy([]);
            $this->cm->store($parametersCache, $parameters);
        }

        return $parameters;
    }
}
