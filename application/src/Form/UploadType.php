<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;

class UploadType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('placesFile', FileType::class, [
          'label' => 'TSV Lieux'
        ])
        
        ->add('mentionsFile', FileType::class, [
          'label' => 'TSV Mentions'
        ])

        ->add('romanFile', FileType::class, [
          'label' => 'Roman'
        ])

        ->add('zip', FileType::class, [
          'mapped' => false,
          'label' => 'Zip Images',
          'attr' => ['accept' => 'application/zip'],
        ])

        ->add('submit', SubmitType::class, array(
            'label' => 'save',
        ));
    }
}
