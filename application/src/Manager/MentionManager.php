<?php

namespace App\Manager;

use App\Entity\Image;
use App\Entity\Mention;
use App\Entity\Place;
use App\Manager\CacheManager;
use Symfony\Component\Filesystem\Filesystem;
use Doctrine\ORM\EntityManagerInterface;

class MentionManager
{
    private $em;
    protected $cm;


    public function __construct(EntityManagerInterface $em, CacheManager $cm)
    {
        $this->em = $em;
        $this->cm = $cm;
    }

    public function getJson($images)
    {
        $mentionsCache = $this->cm->get("mentions.json");

        if ($mentionsCache->isHit()) {
            $mentions = $mentionsCache->get();
        } else {
            $mentions = [];
            foreach ($images as $image) {
                $imageName = $image->getName();
                $mentions[$imageName] = [];
                foreach ($image->getMentions() as $mention) {
                    $place = $mention->getPlace();
                    $alias = $place->getAlias();

                    if ($mention->getCoordinates()) {
                        $coordinates = explode("#", $mention->getCoordinates());
                        if (!isset($mentions[$imageName][$alias])) {
                            $mentions[$imageName][$alias] = [];
                        }
                        $mentions[$imageName][$alias][] = [$coordinates[0], $coordinates[1], $coordinates[2], $coordinates[3]];
                    }
                }
            }

            $this->cm->store($mentionsCache, $mentions);
        }


        return json_encode($mentions);
    }


    public function deleteAll()
    {
        $mentions = $this->em->getRepository(Mention::class)->findAll();
        foreach ($mentions as $mention) {
            $this->em->remove($mention);
        }
        $this->em->flush();

        return;
    }

    public function createImage($name)
    {
        $image = $this->em->getRepository(Image::class)->findOneByName($name);
        if (!$image) {
            $image = new Image;
            $image->setName($name);
            $image->setSvg($name.".svg");
            $image->setThumbnail($name);
            $this->em->persist($image);
            $this->em->flush();
        }

        return $image;
    }

    public function create($type, $coordinates, $caption, Place $place, Image $image, $rank)
    {
        $mention = new Mention;
        $mention->setType($type);
        $mention->setCoordinates($coordinates);
        $mention->setCaption($caption);
        $mention->setPlace($place);
        $mention->setImage($image);
        $mention->setRank($rank);

        $this->em->persist($mention);
        $this->em->flush();

        return $mention;
    }

    public function importMentions($content)
    {
        $arrayLines = explode("\n", $content);
        //1ere ligne à jeter
        //boucler to feof
        //split sur \t => [0] = alias / [1] = name / [2] = description
        $cpt = 0;
        $previousImg = "";
        $rank = 0 ;
        foreach ($arrayLines as $line) {
            if ($cpt !== 0) {
                if (preg_match("/^[^\t]+\t[^\t]+\t[^\t]*\t[^\t]*\t[^\t]*\s*$/", $line)) {
                    $tab = explode("\t", $line);
                    $alias       = $tab[0];
                    $img         = $tab[1];
                    $coordinates = $tab[2];
                    $type        = $tab[3];
                    $caption     = $tab[4];

                    if ($previousImg == $img) {
                        $rank++;
                    } else {
                        $rank = 1;
                    }
                    $previousImg = $img;

                    //get place en fonction de $alias
                    $place = $this->em->getRepository(Place::class)->findOneByAlias($alias);
                    if ($place) {
                        $image = $this->createImage($img);
                        if (!preg_match("/^[^#]+#[^#]+#[^#]+#[^#]+$/", $coordinates)) {
                            $coordinates = null;
                        }
                        $mention = $this->create($type, $coordinates, $caption, $place, $image, $rank);
                    }
                }
            }
            $cpt++;
        }
        $this->em->flush();

        return;
    }

    public function updateMentions($ratio, Image $image)
    {
        $mentions = $image->getMentions();
        foreach ($mentions as $mention) {
            $coordinates = $mention->getCoordinates();
            if (preg_match("/^([^#]+)#([^#]+)#([^#]+)#([^#]+)$/", $coordinates, $arrayMatch)) {
                $x = round(intval($arrayMatch[1]) * $ratio, 0);
                $y = round(intval($arrayMatch[2]) * $ratio, 0);
                $h = round(intval($arrayMatch[3]) * $ratio, 0);
                $w = round(intval($arrayMatch[4]) * $ratio, 0);
                $newCoord = $x."#".$y."#".$h."#".$w;
                $mention->setCoordinates($newCoord);
                $this->em->persist($mention);
            }
        }
        $this->em->flush();
    }

    public function mentionsToTsv($separator, $root)
    {
        $mentions = $this->em->getRepository(Mention::class)->findBy([], ["image" => "ASC", "rank" => "ASC"]);
        $fileContent="identifiant".$separator."fichier".$separator."coordonnées".$separator."type".$separator."légende\n";

        foreach ($mentions as $mention) {
            $fileContent .= $mention->getPlace()->getAlias().$separator.$mention->getImage()->getName().$separator.$mention->getCoordinates().$separator.$mention->getType().$separator.$mention->getCaption()."\n";
        }
        $file = $root."export/mentions.tsv";
        $fileSystem = new Filesystem();
        $fileSystem->dumpFile($file, $fileContent);
        return;
    }
}
