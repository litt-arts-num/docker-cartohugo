<?php

namespace App\Manager;

use App\Entity\Place;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Security\Core\Security;

class PlaceManager
{
    private $em;
    private $security;

    public function __construct(EntityManagerInterface $em, Security $security)
    {
        $this->em = $em;
        $this->security = $security;
    }

    public function deleteAll()
    {
        $places = $this->em->getRepository(Place::class)->findAll();
        foreach ($places as $place) {
            $this->em->remove($place);
        }
        $this->em->flush();

        return;
    }

    public function createPlace($alias, $name, $desc)
    {
        $place = new Place;
        $place->setAlias($alias);
        $place->setName($name);
        $place->setDescription($desc);
        $this->em->persist($place);

        return;
    }

    public function importPlaces($content)
    {
        // $this->removePlaces();
        $arrayLines = explode("\n", $content);

        //1ere ligne à jeter
        //boucler to feof
        //split sur \t => [0] = alias / [1] = name / [2] = description
        $cpt=0;
        foreach ($arrayLines as $line) {
            if ($cpt !== 0) {
                if (preg_match("/^[^\t]+\t[^\t]+\t[^\t]*\t[^\t]*\s*$/", $line)) {
                    $tab = explode("\t", $line);
                    $alias  = $tab[0];
                    $name   = $tab[1];
                    $desc   = $tab[2];
                    $placesAmbiguous = $this->em->getRepository(Place::class)->findByAlias($alias);
                    //si alias deja present, on supprime les précédentes occurences
                    if ($placesAmbiguous) {
                        foreach ($placesAmbiguous as $placeMulti) {
                            $this->em->remove($placeMulti);
                        }
                    }

                    $this->createPlace($alias, $name, $desc);
                }
            }
            $cpt++;
        }
        $this->em->flush();
    }

    public function placesToTsv($separator, $root)
    {
        $places = $this->em->getRepository(Place::class)->findAll();
        $fileContent = "identifiant".$separator."titre/nom".$separator."description".$separator."etc.\n";
        foreach ($places as $place) {
            $fileContent .= $place->getAlias().$separator.$place->getName().$separator.$place->getDescription()."\t\n";
        }
        $file = $root."export/lieux.tsv";
        $fileSystem = new Filesystem();
        $fileSystem->dumpFile($file, $fileContent);

        return;
    }
}
