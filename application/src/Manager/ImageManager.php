<?php

namespace App\Manager;

use App\Entity\Image;
use App\Entity\Mention;
use App\Entity\Place;
use App\Manager\MentionManager;
use Doctrine\ORM\EntityManagerInterface;
use Imagine\Gd\Imagine;
use Imagine\Image\Box;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Filesystem\Filesystem;

class ImageManager
{
    private $em;
    private $params;
    private $mm;
    private $imagine;
    private const MAX_WIDTH = 1280;
    private const MAX_HEIGHT = 1280;

    public function __construct(EntityManagerInterface $em, ParameterBagInterface $params, MentionManager $mm)
    {
        $this->em = $em;
        $this->params = $params;
        $this->mm = $mm;
        $this->imagine = new Imagine();
    }

    public function replace($uploadPath, $filename)
    {
        $filesystem = new Filesystem();
        $image = $this->em->getRepository(Image::class)->findOneByName($filename);
        $tmpRoot = $this->params->get("kernel.project_dir") . "/public/data/tmp";
        $oldRoot = $this->params->get("kernel.project_dir") . "/public/data/images";
        if ($image) {
            // image existante
            $oldImgPath = $oldRoot . DIRECTORY_SEPARATOR . $image->getName();
            list($oldWidth, $oldHeight) = getimagesize($oldImgPath);
            // image uploadée
            $newImgPath = $tmpRoot . DIRECTORY_SEPARATOR . $image->getName();
            list($newWidth, $newHeight) = getimagesize($newImgPath);
            $ratio = $newWidth / $oldWidth;
            if ($ratio != 1) {
                $this->mm->updateMentions($ratio, $image);
                $filesystem->remove($oldImgPath);
                $filesystem->rename($newImgPath, $oldImgPath);
                $this->updateSVG($image);
            }
        }
        $filesystem->remove($tmpRoot);
    }

    public function deleteAll()
    {
        $images = $this->em->getRepository(Image::class)->findAll();
        foreach ($images as $image) {
            $this->em->remove($image);
        }

        $this->em->flush();

        return;
    }

    public function updateSVG(Image $image)
    {
        $root = $this->params->get("kernel.project_dir") . "/public/data/images";
        $cssFile = "../../css/svg.css";
        $imgPath = $root.DIRECTORY_SEPARATOR.$image->getName();
        $svgFilePath = $imgPath.".svg";

        dump($image);
        dump($image->getName());
        if (file_exists($imgPath)) {
            list($width, $height) = getimagesize($imgPath);

            $svgFileContent = "<?xml version='1.0' encoding='UTF-8'?>\n<?xml-stylesheet href='".$cssFile."'?>
            <svg xmlns='http://www.w3.org/2000/svg' xmlns:cc='http://creativecommons.org/ns#' xmlns:xlink='http://www.w3.org/1999/xlink' xmlns:dc='http://purl.org/dc/elements/1.1/' xmlns:tei='http://www.tei-c.org/ns/1.0' width='".$width."' height='".$height."' viewBox='0 0 ".$width." ".$height."'>";

            $svgFileContent .= "<image xlink:href='".$image->getName().".small.jpg' height='".$height."' width='".$width."' />";

            $mentions = $this->em->getRepository(Mention::class)->findByImage($image);

            foreach ($mentions as $mention) {
                $place = $mention->getPlace();
                $alias = $place->getAlias();
                if ($mention->getCoordinates() && $mention->getCoordinates() != "") {
                    $coordinates = explode("#", $mention->getCoordinates());
                    $svgFileContent .= $this->generateRect($place, $coordinates[0], $coordinates[1], $coordinates[2], $coordinates[3]);
                } else {
                    $svgFileContent .= $this->generateRect($place, 0, 0, 0, 0);
                }
            }
            $svgFileContent .= "</svg>";
            file_put_contents($svgFilePath, $svgFileContent);
        }
    }

    public function generateSmallerImages()
    {
        $images = $this->em->getRepository(Image::class)->findAll();
        $root = $this->params->get("kernel.project_dir") . "/public/data/images";

        foreach ($images as $image) {
            $imgPath = $root.DIRECTORY_SEPARATOR.$image->getName();
            list($iwidth, $iheight) = getimagesize($imgPath);
            $ratio = $iwidth / $iheight;
            $width = self::MAX_WIDTH;
            $height = self::MAX_HEIGHT;
            if ($width / $height > $ratio) {
                $width = $height * $ratio;
            } else {
                $height = $width / $ratio;
            }

            $photo = $this->imagine->open($imgPath);
            $photo->resize(new Box($width, $height))->save($imgPath.".small.jpg");
        }

        return;
    }

    public function generateSVGs()
    {
        $images = $this->em->getRepository(Image::class)->findAll();

        foreach ($images as $image) {
            $this->updateSVG($image);
        }

        return;
    }

    public function generateRect(Place $place, $x, $y, $height, $width)
    {
        return "<g class='location location-hidden' data-location='".$place->getAlias()."'>
                <title>".$place->getName()."</title>
                <rect width='".$height."' height='".$width."' x='".$x."' y='".$y."'/>
                </g>";
    }

    public function exportImages($root)
    {
        $zip = new \ZipArchive();
        $ret = $zip->open($root.'export/images.zip', \ZipArchive::CREATE | \ZipArchive::OVERWRITE);
        if ($ret !== true) {
            printf("A échoué avec le code d'erreur %d", $ret);
        } else {
            $options = ['remove_all_path' => true];
            $images = $this->em->getRepository(Image::class)->findAll();
            foreach ($images as $image) {
                $imgPath = $root.DIRECTORY_SEPARATOR."data/images".DIRECTORY_SEPARATOR.$image->getName();
                $zip->addGlob($imgPath, GLOB_BRACE, $options);
            }
            $zip->close();
        }

        return;
    }
}
