<?php

namespace App\Manager;

use App\Entity\Roman;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Security\Core\Security;

class RomanManager
{
    private $em;
    private $security;

    public function __construct(EntityManagerInterface $em, Security $security)
    {
        $this->em = $em;
        $this->security = $security;
    }

    public function deleteAll()
    {
        $romans = $this->em->getRepository(Roman::class)->findAll();
        foreach ($romans as $roman) {
            $this->em->remove($roman);
        }
        $this->em->flush();

        return;
    }

    public function createRoman($content)
    {
        $roman = new Roman;
        $roman->setContent($content);
        $this->em->persist($roman);
        $this->em->flush();

        return;
    }

    public function romanToFile($root)
    {
        $romans = $this->em->getRepository(Roman::class)->findAll();
        $romanContent = $romans[0]->getContent();
        $file = $root."export/travailleurs-de-la-mer.txt";
        $fileSystem = new Filesystem();
        $fileSystem->dumpFile($file, $romanContent);

        return;
    }
}
