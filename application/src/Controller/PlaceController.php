<?php

namespace App\Controller;


use App\Entity\Place;
use App\Manager\CacheManager;
use App\Manager\PlaceManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/places", name="places_")
 */
class PlaceController extends AbstractController
{
    protected $cm;
    protected $pm;
    protected $em;

    public function __construct(CacheManager $cm, PlaceManager $pm, EntityManagerInterface $em)
    {
        $this->cm = $cm;
        $this->pm = $pm;
        $this->em = $em;
    }

    /**
     * @Route("/all", name="all")
     */
    public function index()
    {
        $places = $this->em->getRepository(Place::class)->findBy([], ["name" => "ASC"]);

        return $this->render('places.html.twig', [
          "places" => $places
        ]);
    }


}
