<?php

namespace App\Controller;

use App\Entity\Parameters;
use App\Entity\Roman;
use App\Entity\User;
use App\Form\ImagesType;
use App\Form\ParametersType;
use App\Form\UploadType;
use App\Manager\CacheManager;
use App\Manager\ImageManager;
use App\Manager\MentionManager;
use App\Manager\PlaceManager;
use App\Manager\RomanManager;
use App\Manager\UserManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\HeaderUtils;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * @Route("/admin", name="admin_")
 */
class AdminController extends AbstractController
{
    protected $cm;
    protected $um;
    protected $pm;
    protected $mm;
    protected $im;
    protected $rm;

    public function __construct(CacheManager $cm, UserManager $um, PlaceManager $pm, MentionManager $mm, ImageManager $im, RomanManager $rm)
    {
        $this->cm = $cm;
        $this->um = $um;
        $this->pm = $pm;
        $this->mm = $mm;
        $this->im = $im;
        $this->rm = $rm;
    }

    /**
     * @Route("/users", name="list_users")
     */
    public function index()
    {
        $users = $this->um->getUsers();

        return $this->render('admin/users.html.twig', [
          "users" => $users
        ]);
    }

    /**
     * @Route("/user/role/{id}/{role}", name="set_role")
     */
    public function setRole(User $user, $role)
    {
        $this->um->setRole($user, [$role]);
        $this->addFlash('success', "switch admin done");

        return $this->redirectToRoute('admin_list_users');
    }

    /**
     * @Route("/user/{id}", name="delete_user")
     */
    public function deleteUser(User $user)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($user);
        $em->flush();

        $this->addFlash('success', "utilisateur supprimé");
        $this->cm->clear("user.all");

        return $this->redirectToRoute('admin_list_users');
    }

    /**
     * @Route("/parameters", name="parameters")
     */
    public function parameters(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        if ($parameters = $em->getRepository(Parameters::class)->findOneBy([])) {
            $form = $this->createForm(ParametersType::class, $parameters);
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $request = $request->request;
                $em->persist($parameters);
                $em->flush();
                $this->cm->clear("parameters");

                $this->addFlash('notice', 'Paramètres modifiés');

                return $this->redirectToRoute('homepage');
            }

            return $this->render('admin/parameters.html.twig', [
              "form" => $form->createView()
            ]);
        }

        return $this->redirectToRoute('homepage');
    }

    /**
     * @Route("/users/export-datas", name="export-datas")
     */
    public function exportDatas()
    {
        $filesystem = new Filesystem;
        $root = $this->getParameter("kernel.project_dir") . "/public/";
        $separator = "\t";
        //generate zip with images zip + tsv lieux & tsv mentions
        $imagesZip = $this->im->exportImages($root);
        $placesFile = $this->pm->placesToTsv($separator, $root);
        $mentionsFile = $this->mm->mentionsToTsv($separator, $root);
        $roman = $this->rm->romanToFile($root);

        $zip = new \ZipArchive();
        $zipName = $root.'export.zip';
        $ret = $zip->open($zipName, \ZipArchive::CREATE | \ZipArchive::OVERWRITE);
        if ($ret !== true) {
            printf("A échoué avec le code d'erreur %d", $ret);
        } else {
            $options = ['add_path' => $root, 'remove_all_path' => true];
            $zip->addGlob($root.'export/*', GLOB_BRACE, $options);
            $zip->close();
        }

        $response = new Response(file_get_contents($zipName));
        $disposition = HeaderUtils::makeDisposition(
            HeaderUtils::DISPOSITION_ATTACHMENT,
            "export-cartohugo.zip"
        );
        $response->headers->set('Content-Type', 'application/zip');
        $response->headers->set('Content-Disposition', $disposition);
        $filesystem->remove([$root.'export/mentions.tsv', $root.'export/images.zip', $root.'export/lieux.tsv', $root.'export/travailleurs-de-la-mer.txt']);
        $filesystem->remove($root.'export.zip');

        return $response;
    }

    /**
     * @Route("/users/export", name="export_users")
     */
    public function exportUsers()
    {
        $encoders = [new CsvEncoder(";")];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);

        $users = $this->um->getUsers();
        $fileSystem = new Filesystem();
        $root = $this->getParameter("kernel.project_dir") . "/public/";
        $file = "export/" . uniqid() . ".csv";
        $csvContent = $serializer->serialize($users, 'csv');
        $fileSystem->appendToFile($root.$file, $csvContent);
        $this->addFlash('success', "export OK");

        return $this->render('admin/users.html.twig', [
         "users" => $users,
         "file" => $file
        ]);
    }

    /**
     * @Route("/data/upload-images", name="upload-images")
     */
    public function uploadImages(Request $request)
    {
        $form = $this->createForm(ImagesType::class);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $uploadPath = $this->getParameter("kernel.project_dir") . "/public/data/tmp";
            $zipFile = $form->get('zip')->getData();

            $zipName = $zipFile->getClientOriginalName();
            $zipFile->move($uploadPath, $zipName);

            $zip = new \ZipArchive;
            $success = $zip->open($uploadPath . DIRECTORY_SEPARATOR . $zipName);
            if ($success === true) {
                $zip->extractTo($uploadPath);
                $zip->close();

                $iterator = new \ RecursiveIteratorIterator(new \RecursiveDirectoryIterator($uploadPath, \FilesystemIterator::SKIP_DOTS), \RecursiveIteratorIterator::SELF_FIRST);
                foreach ($iterator as $filename => $fileInfo) {
                    $name = basename($filename);
                    if ($fileInfo->isFile()) {
                        if (preg_match("/\.(jpg|jpeg|png)$/", substr($name, -5))) {
                            $this->im->replace($uploadPath, $name);
                        }
                    }
                }
            }
            $this->cm->clear("mentions.json");
            $this->addFlash('notice', "Images ajoutées");
            return $this->redirectToRoute('homepage');
        }

        return $this->render('admin/upload-images.html.twig', [
        "form" => $form->createView()
      ]);
    }

    /**
     * @Route("/data/upload-data", name="upload-datas")
     */
    public function uploadDatas(Request $request)
    {
        $form = $this->createForm(UploadType::class);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->cm->clear("mentions.json");

            // Suppression des anciennes données
            $this->mm->deleteAll();
            $this->im->deleteAll();
            $this->pm->deleteAll();
            $this->rm->deleteAll();

            $request = $request->request;

            $lieuxFile = $form->get('placesFile')->getData();
            $content = file_get_contents($lieuxFile->getPathname());
            $this->pm->importPlaces($content);
            $this->addFlash('notice', "Lieux ajoutés");

            $mentionsFile = $form->get('mentionsFile')->getData();
            $content = file_get_contents($mentionsFile->getPathname());
            $this->mm->importMentions($content);

            $this->addFlash('notice', "Mentions ajoutées");

            $uploadPath = $this->getParameter("kernel.project_dir") . "/public/data/images";
            $zipFile = $form->get('zip')->getData();
            $zipName = $zipFile->getClientOriginalName();
            $zipFile->move($uploadPath, $zipName);

            $zip = new \ZipArchive;
            $success = $zip->open($uploadPath . DIRECTORY_SEPARATOR . $zipName);
            if ($success === true) {
                $zip->extractTo($uploadPath);
                $zip->close();
            }

            $this->im->generateSmallerImages();
            $this->im->generateSVGs();

            $this->addFlash('notice', "Images ajoutées");

            $romanFile = $form->get('romanFile')->getData();
            $content = file_get_contents($romanFile->getPathname());
            $this->rm->createRoman($content);

            $this->addFlash('notice', "Roman ajouté");

            return $this->redirectToRoute('homepage');
        }

        return $this->render('admin/upload-datas.html.twig', [
          "form" => $form->createView()
        ]);
    }
}
