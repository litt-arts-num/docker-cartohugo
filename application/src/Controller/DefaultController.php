<?php

namespace App\Controller;

use App\Entity\Image;
use App\Entity\Place;
use App\Entity\Roman;
use App\Manager\MentionManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="homepage")
     */
    public function index(MentionManager $mentionManager)
    {
        $places = $this->getDoctrine()->getRepository(Place::class)->findBy([], ["name" => "ASC"]);
        $images = $this->getDoctrine()->getRepository(Image::class)->findAll();
        $romans = $this->getDoctrine()->getRepository(Roman::class)->findAll();
        $mentionsJson = $mentionManager->getJson($images);

        return $this->render('default/index.html.twig', [
          "places" => $places,
          "images" => $images,
          "romans" => $romans,
          "jsonMention" => $mentionsJson
        ]);
    }

    /**
     * @Route("/mention-legales", name="legal_notices")
     */
    public function legalNotices()
    {
        return $this->render('default/legal-notices.html.twig');
    }
}
