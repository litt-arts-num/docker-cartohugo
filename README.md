# CARTO-HUGO

## Installation

* cloner le projet
* se placer dans le répertoire
* lancer `make init` (en sudo ou pas selon si votre utilisateur est dans le groupe docker)

  * le premier fichier qui sera proposé à l'édition sera le .env dont se servira docker-compose. Il faut définir les ports d'apache et adminer, ainsi que des variables concernant mysql
  * le second fichier est application/.env propre à symfony. Il faut y répercuter (voir ligne `DATABASE_URL`) les variables entrées dans le 1er fichier.
  * ensuite se lance le build des images docker
  * enfin, l'application symfony s'initialise.
* l'application devrait être disponible sur `localhost:XXX` où `XXX` est le port défini pour apache.
  * créer un compte
  * lui donner les droits via la commande `console app:toggleadmin <mail> ROLE_SUPER_ADMIN` depuis le container apache. On s'y connecte via `docker-compose exec apache bash`

## Mise à jour
   ```
  git pull origin master
  docker-compose exec apache make update
  ```
